package com.mobile.ict.cart.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mobile.ict.cart.LokacartApplication;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.container.Organisations;
import com.mobile.ict.cart.util.GetJSON;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.Material;

import org.json.JSONException;
import org.json.JSONObject;

public class OrgInfoActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tvInfoOrgName;
    TextView tvInfoOrgAbout;
    FrameLayout flOrginfoLogoLargeContainer;
    FrameLayout rootContainer;
    ImageView iVInfoOrgLogo,iVInfoOrgLogoLarge;
    static String orgAbbr;

    private Tracker mTracker;
    private static final String TAG = "OrgInfoActivity";
    String name = "OrgInfo";

    String logoUrl = "";
    String number="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Obtain the shared Tracker instance.
        LokacartApplication application = (LokacartApplication) getApplication();
        mTracker = application.getDefaultTracker();
        Log.i(TAG, "Setting screen name: " + name);
        mTracker.setScreenName("Screen~" + name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        setContentView(R.layout.activity_org_info);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getString(R.string.title_activity_Org_info));
        tvInfoOrgAbout = (TextView)findViewById(R.id.tvOrginfoabout);
        tvInfoOrgName = (TextView)findViewById(R.id.tvOrginfoName);
        iVInfoOrgLogo  = (ImageView)findViewById(R.id.ivOrginfoLogo);
        iVInfoOrgLogoLarge  = (ImageView)findViewById(R.id.ivOrginfoLogoLarge);
        flOrginfoLogoLargeContainer = (FrameLayout)findViewById(R.id.flOrginfoLogoLargeContainer);
        rootContainer = (FrameLayout) findViewById(R.id.rootcontainer);

        iVInfoOrgLogo.setOnClickListener(this);
        iVInfoOrgLogoLarge.setOnClickListener(this);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.orginfofab);
        fab.setOnClickListener(this);
        number = getIntent().getStringExtra("contact");
        orgAbbr =  getIntent().getStringExtra("abbr");
        tvInfoOrgName.setText(getIntent().getStringExtra("name"));
        JSONObject params = new JSONObject();
        try {
            params.put("r","e");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new getOrgInfo().execute(params);

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.ivOrginfoLogo:{
                flOrginfoLogoLargeContainer.setVisibility(View.VISIBLE);
                rootContainer.setBackgroundColor(0xc7c7c7);
                break;
            }
            case R.id.ivOrginfoLogoLarge:{

//                Dialog authenticationDialogView = new Dialog(OrgInfoActivity.this);
//                authenticationDialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);
//
//                authenticationDialogView.setContentView(R.layout.user_authentication);
//
                flOrginfoLogoLargeContainer.setVisibility(View.GONE);
                rootContainer.setBackgroundColor(0x7775);
//                if(logoUrl.equals("")||logoUrl.equals("null")){
//
//
//
//                }else{
//
//                    Glide.with(OrgInfoActivity.this).load(logoUrl)
//                            .thumbnail(0.5f)
//                            .crossFade()
//                            .placeholder(R.drawable.organisation_placeholder)
//                            .diskCacheStrategy(DiskCacheStrategy.ALL)
//                            .into(iVInfoOrgLogo);
//
//                }
                break;
            }
            case R.id.orginfofab:{

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:+"+number));
                startActivity(intent);
                break;
            }
        }
    }


    public class getOrgInfo extends AsyncTask<JSONObject,String,String>{


        ProgressDialog pd;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            FlurryAgent.logEvent("getOrgInfoTask");
            Material.circularProgressDialog(OrgInfoActivity.this, getString(R.string.pd_getting_org_info), false);
        }

        @Override
        protected String doInBackground(JSONObject... params) {

            GetJSON getJson = new GetJSON();
            Log.d("orginfo url" , Master.getOrgInfoURL(orgAbbr));
            Master.response = getJson.getJSONFromUrl(Master.getOrgInfoURL(orgAbbr), null, "GET", true, MemberDetails.getEmail(), MemberDetails.getPassword());
            return Master.response;
        }

        @Override
        protected void onPostExecute(String response) {
            Log.d("orgInfo", response.toString());
            if(Material.circularProgressDialog.isShowing())
                Material.circularProgressDialog.dismiss();

            if(response.equals("exception")){



            }else{
                try {
                    JSONObject root = new JSONObject(response);
                    tvInfoOrgAbout.setText(root.getString("description"));
                    logoUrl =  root.getString("logoUrl");
                    Glide.with(OrgInfoActivity.this).load(logoUrl)
                            .thumbnail(0.5f)
                            .crossFade()
                            .placeholder(R.drawable.organisation_placeholder)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(iVInfoOrgLogo);

                    Glide.with(OrgInfoActivity.this).load(logoUrl)
                            .thumbnail(0.5f)
                            .crossFade()
                            .placeholder(R.drawable.organisation_placeholder)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(iVInfoOrgLogoLarge);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
