package com.mobile.ict.cart.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mobile.ict.cart.LokacartApplication;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.fragment.AboutUsFragment;
import com.mobile.ict.cart.fragment.FAQFragment;
import com.mobile.ict.cart.fragment.TermsAndConditionsFragment;
import com.mobile.ict.cart.util.Master;

public class InfoPage extends AppCompatActivity {

    private Tracker mTracker;
    private static final String TAG = "InfoPage";
    String name = "Info";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Obtain the shared Tracker instance.
        LokacartApplication application = (LokacartApplication) getApplication();
        mTracker = application.getDefaultTracker();
        Log.i(TAG, "Setting screen name: " + name);
        mTracker.setScreenName("Screen~" + name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        setContentView(R.layout.activity_info_page);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String open = getIntent().getStringExtra("open");
        if(open.equals("faq")){
            getSupportFragmentManager().beginTransaction().replace(R.id.info_container, new FAQFragment(), Master.FAQ_TAG).commit();

        }else if(open.equals("tnc")){
            getSupportFragmentManager().beginTransaction().replace(R.id.info_container, new TermsAndConditionsFragment(), Master.TERMS_AND_CONDITIONS_TAG).commit();

        }else{
            getSupportFragmentManager().beginTransaction().replace(R.id.info_container, new AboutUsFragment(), Master.ABOUT_US_TAG).commit();

        }


    }
}
